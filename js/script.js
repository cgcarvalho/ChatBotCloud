const urlLuis = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/e1413f75-e39f-4147-920e-86e1f701402a?subscription-key=8833a11e64a5466ba9baa9e9396f1658&verbose=true&timezoneOffset=0&q=";
const inputMensagem = document.querySelector(".message_input");
const boatoEnviar = document.querySelector(".send_message");
const template = document.querySelector(".message_template");
const conversa = document.querySelector(".messages");
let ladoMensagem = 'right';

function checkEnter(e){
	if (e.which === 13) {
		preparaMensagem();
    }
}

function preparaMensagem(){
	let texto = inputMensagem.value;
    enviarMensagemUser(texto);
}

function enviarMensagemUser(texto){
    enviarMensagem(texto, 'user');
    if(!checkEasterEgg(texto)){
        mensagemChat(texto);
    }
}

function enviarMensagemBot(texto){
    enviarMensagem(texto, 'bot')
    falar(texto);
}

function checkEasterEgg(texto){
    switch(texto){
        case "Você conhece a Bia":
            enviarMensagemBot("Nunca ouvi falar.")
            return true;
        case "quero falar com o legado":
            enviarMensagemBot("Peraí que eu vou chamar o Mendes.")
            return true;
        case "tudo bem":
        case "tudo certo":
            enviarMensagemBot("Topizeira")
            return true;
        case "obrigado":
        case "obrigada":
        case "valeu":
        case "tchau":
            enviarMensagemBot("Tamo junto")
            return true;  
        default:
            return false;
    }
}

function checkIntent(intent){
    switch(intent){
        case "AberturaConta":
            enviarMensagemBot("Você quer abrir uma conta");
            break;
        case "Investimentos":
            enviarMensagemBot("Você quer ver seus investimentos");
            break;
        case "Saldo":
            enviarMensagemBot("Você quer ver seu saldo");
            break;
        case "Transferencia":
            enviarMensagemBot("Você quer fazer uma trasnferência");
            break;
        default:
            enviarMensagemBot("Não consigo te ajudar com isso, mas vou estudar para aprender.");
            break;
    }
}

function enviarMensagem(texto, ladoMensagem){
    let novaMensagem = criarNovaMensagem(texto);
    novaMensagem.className = "message " + ladoMensagem + " appeared";
    
    conversa.appendChild(novaMensagem);

    conversa.scrollTop += 300;
    inputMensagem.value = "";
}

function criarNovaMensagem(textoMensagem){
    let corpo = document.createElement("li");
    corpo.className = "message";

    let avatar = document.createElement("div");
    avatar.className = "avatar";
  
    let textWraper = document.createElement("div");
    textWraper.className = "text_wrapper"; 
   
    let texto = document.createElement("div");
    texto.className = "text"; 
    texto.innerHTML = textoMensagem;
    
    corpo.appendChild(avatar);
    textWraper.appendChild(texto);
    corpo.appendChild(textWraper);

    return corpo;
}


function mensagemChat(data){
	 fetch(urlLuis, 
			 {
		 		method: 'POST',  
		 		headers: {
	 		      'Content-type': 'application/json'
	 		    },
		 		body: JSON.stringify(data)
	 		})
	 .then(extrairJSON)
     .then(mensagemChatResposta);
}
function mensagemChatResposta(resposta){
    if(resposta.topScoringIntent.score > 0.6)
        checkIntent(resposta.topScoringIntent.intent);
    else
        enviarMensagemBot("Não consigo te ajudar com isso, mas vou estudar para aprender.");
}

function extrairJSON(resposta){
    return resposta.json();
}



//Voz
const btnConversar = document.querySelector('.bottom_wrapper_gravar');

window.SpeechRecognition = window.SpeechRecognition || webkitSpeechRecognition;
let trigger = new SpeechRecognition();
trigger.continuous = false;
trigger.lang = 'pt-BR';


let vozInicial = true;
let parar = false;

eventosRecognition();
ouvirTrigger();
speechSynthesis.getVoices();

window.speechSynthesis.onvoiceschanged = function() {
    if(vozInicial){
        enviarMensagemBot("Olá, em que posso ajudar?");
        vozInicial = false;
    }
};

function ouvirTrigger() {
    trigger.start();
    trigger.onresult = (event) => {
      let speechToText = Array.from(event.results)
      .map(result => result[0])
      .map(result => result.transcript)
      .join('');

      if (event.results[0].isFinal) {
        let triggerWord = ehTrigger(speechToText);
        if(triggerWord != "null"){
            enviarMensagemUser(speechToText.toLowerCase().replace(triggerWord + " ", ""));
        }
      }
    }
}


function ehTrigger(speech) {
    let triggers = ["e aí itaú", "e ai itau", "e aí tá", "e aí tal", "e aí tava"];

    for(let i of triggers){
        if(speech.toLowerCase().includes(i))
            return i;
    }
    return "null";
}


function falar(speech) {
    const utterThis = new SpeechSynthesisUtterance(speech);
    utterThis.voice = speechSynthesis.getVoices()[17];
    window.speechSynthesis.speak(utterThis);
  }


 function tratarConversaClick() {
      parar = true;
      trigger.stop();
  }

  function eventosRecognition() {
    trigger.onstart = function() {
    };
    
    trigger.onend = function() {
        if(!parar)
            ouvirTrigger();
    };
  }


//Eventos
btnConversar.onclick = tratarConversaClick;
inputMensagem.onkeyup = checkEnter;
boatoEnviar.onclick = preparaMensagem;
